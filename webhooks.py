#!/usr/bin/env python3
"""
Simple HTTP server to process bitbucket webhook events.
This thing should be as simple as possible using only stdlib

Usage:
    python3 webhooks.py -a <bind-address> -p <bind-port>

Examples:
    python3 webhooks.py # (will listen at 127.0.0.1:8080 by default)
    python3 webhooks.py -a 10.3.1.3 -p 5555

Be sure to rename config-example.py to config.py and set it accordingly
"""

import json
import shlex
import logging
from sys import stderr
from subprocess import call
from http.server import HTTPServer, BaseHTTPRequestHandler

# Load config:
try:
    import config
except ImportError as error:
    error.msg = "Coudln't load config file. Is config.py properly configured?"
    raise

# Set up logger:
logger = logging.getLogger(__name__)
logging.basicConfig(filename=config.logging['path'],
                    level=config.logging['level'],
                    format='%(message)s')


class BitBucketHTTPHandler(BaseHTTPRequestHandler):

    def do_POST(self):
        """
        Manages POST requests. All other HTTP methods will return 501.
        """

        logger.debug("{} HTTP command received. ".format(self.raw_requestline))

        content_type = self.headers['Content-Type']
        msg_length = int(self.headers['Content-Length'])
        event_key = self.headers['X-Event-Key']

        try:
            # Some sanity checks:
            if content_type != 'application/json':
                raise ValueError("Payload must be in json format")
            if event_key is None:
                raise ValueError("Malformed webhook headers")

            # Extract POST payload:
            raw_content = self.rfile.read(msg_length)
            post_data = json.loads(raw_content.decode())

            # Process webhook events based on event_key:
            if event_key == 'repo:push':
                self.process_push(post_data)
            else:
                raise NotImplementedError  # TODO: Implement other events processing

            self.send_response(200, "Webhook successfully processed")

        except ValueError as exception:
            print(exception, file=stderr)
            self.send_error(400)

        except NotImplementedError:
            print("Webhook event {} is not implemented".format(event_key))
            self.send_error(501)

        except OSError as exception:
            print(exception, file=stderr)
            self.send_error(500, explain="Webhook command {} execution failed".format(config.command))

        except Exception as exception:
            print(exception)
            self.send_error(500, explain="Webhook command {} returned != 0".format(config.command))

        finally:
            self.end_headers()

    @staticmethod
    def process_push(post_data):
        """
        Process webhook payload and executes command. Reference:
        https://confluence.atlassian.com/display/BITBUCKET/Event+Payloads
        """

        for change in post_data['push']['changes']:
                if change['new']['name'] == config.branch_name:
                    retcode = call(shlex.split(config.command), cwd=config.working_dir)
                    if retcode != 0:
                        raise Exception("Failed to run {}".format(config.command))

    def log_message(self, format, *args):
        """
        Log a message
        """

        logger.info("%s - - [%s] %s" %
                    (self.address_string(),
                     self.log_date_time_string(),
                     format % args))


if __name__ == '__main__':
    import argparse

    # Handle command line args:
    parser = argparse.ArgumentParser(description='Bitbucket webhook app')
    parser.add_argument('-a', '--address', help='default: 127.0.0.1')
    parser.add_argument('-p', '--port', help='default: 8080', type=int)
    args = parser.parse_args()

    # Serve webhooks:
    server = HTTPServer((args.address or '127.0.0.1', args.port or 8080), BitBucketHTTPHandler)
    server.serve_forever()
