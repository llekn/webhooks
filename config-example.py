# Example config file for webhooks.
# Rename me to config.py

from logging import CRITICAL, ERROR, WARNING, INFO, DEBUG

branch_name = 'master'  # Branch to listen to
command = 'git pull origin master'  # Command to be executed on webhook processing
working_dir = '.'  # Path where command will be executed

logging = {'path': 'webhook.log',
           'level': INFO  # possibilites are: CRITICAL, ERROR, WARNING, INFO, DEBUG
           }

