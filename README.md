# Bitbucket webhooks

This aims to be a *really* simple app to process Bitbucket's web hooks.

Why another one? Well, I wanted something simpler than existing solutions that didn't require installing
web frameworks.

## Project goals
+ Work with Bitbucket's webhooks
+ Be able to run an arbitrary command
+ Be really simple (use only stdlib)

## Usage
This is intended to be used as web application behind a web server (like nginx).


### Add a new route to your web server.

For nginx add on your site config (i.e. `/etc/nginx/sites-enabled/default`):
```
     location /your_webhook_path {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_pass http://127.0.0.1:8080;
     }

```

### Configure webhooks:
 Rename config-example.py to config.py and set:
 - `branch_name` Branch to listen to
 - `command` Command to be executed on webhook processing
 - `repo_path` Path where command will be executed
 - `log['file']` File where to log incoming requests
 - `log['level']` Logging verbosity level. Options are `CRITICAL`, `ERROR`, `WARNING`, `INFO`, `DEBUG`

### Run webhooks:
```
    python3 webhooks.py # This will run webhooks on 127.0.0.1:8080
```
It is possible to run webhooks on an arbitrary ip address and port, for example

```
     python3 webhook-listener.py -a 10.4.1.23 -p 5000
```

Running webhooks in this way will print executed commands to stdout and will log HTTP requests to log file.

You would probably like to run webhooks forever. My preferred option is to use `nohup`:

```
     nohup python3 webhooks.py -a 10.4.1.23 -p 5000 > commands.log &
```